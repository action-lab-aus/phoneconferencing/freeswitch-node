const fs = require('fs');
const langMapper = require('./language_mapper.js');
const uploadsRoot = 'recordings'
const trunkStatusAddress = 'System/Trunks';
const newJobsAddress = 'Calljobs/New';
const processedJobsAddress = 'Calljobs/Processed';
const logsRoot = 'FreeswitchLogs';
const dialplan_consentThenJoin = '8675310';
const dialplan_join = 69420;

class DbManager 
{
    static callManager = null;
    static soundsManager = null;
    static db = null;
    static storage = null;

    static ongoingCalls = {};
    static ongoingConfs = {};

    static sleep(ms)
    {
        return new Promise((resolve) =>
        {
            setTimeout(resolve, ms);
        });
    }

    static async startMonitoring()
    {
        let envTrunks = DbManager.callManager.getTrunkDetails();
        let dbTrunks = (await DbManager.db.ref(trunkStatusAddress).once("value")).val();

        console.log('Checking for trunk updates...')

        envTrunks.forEach(async trunk =>
        {
            let match = dbTrunks?.[trunk.caller_number];

            if (!match || (match.updated < trunk.updated))
            {
                // Env file has updated trunk details.
                // Strip of sensitive information before updating the db
                delete trunk.username;
                delete trunk.password;
                await DbManager.db.ref(`${trunkStatusAddress}/${trunk.caller_number}`).update(trunk);
                console.log('Updated ' + trunk.gateway);
            }
            else
            {
                console.log(trunk.gateway + ' up to date');
            }

        });

        var monitorRef = DbManager.db.ref(newJobsAddress);
        monitorRef.on('value', (snapshot) =>
        {
            const newJobs = snapshot.val();

            for (var key in newJobs)
            {
                var thisJob = newJobs[key];
                thisJob.id = key;

                if (thisJob.Status !== "ready") continue;

                switch (thisJob.Task)
                {
                    case 'call':
                        DbManager.requestStartCall(thisJob);
                        break;
                    case 'hangup':
                        DbManager.requestHangupCall(thisJob);
                        break;
                    case 'mute':
                    case 'unmute':
                        DbManager.requestMuteUnmuteCaller(thisJob);
                        break;
                    case 'muteAll':
                    case 'unmuteAll':
                        DbManager.requestMuteUnmuteAll(thisJob);
                        break;
                    case 'endConf':
                        DbManager.requestEndConference(thisJob);
                        break;
                    case 'breakOut':
                        console.log('breakout task', thisJob);
                        break;
                    default:
                        console.log("Unhandled calljob task type: " + thisJob.task)
                }
            }
        });
    }

    static async checkIsModerator(confId, number)
    {
        if (number == "1000") return true;

        let confRes = await DbManager.findConf(confId);
        if (!confRes) throw 'Conference not found'

        var participant = await DbManager.db.ref(`${confRes.path}/Participants/${number}`).once("value");
        return participant.exists() ? participant.val().Moderator : false;
    }

    static async findConf(confId)
    {
        if (!DbManager.ongoingConfs[confId])
        {
            // prioritise Complete confs if we think they're not ongoing
            thisConf = await DbManager.db.ref(`Conferences/Complete/${confId}`).once("value");
            if (thisConf.exists()) return { 'path': `Conferences/Complete/${confId}`, 'res': thisConf, 'id': confId };
        }

        var thisConf = await DbManager.db.ref(`Conferences/Live/${confId}`).once("value");
        if (thisConf.exists()) return { 'path': `Conferences/Live/${confId}`, 'res': thisConf, 'id': confId };

        thisConf = await DbManager.db.ref(`Conferences/Upcoming/${confId}`).once("value");
        if (thisConf.exists()) return { 'path': `Conferences/Upcoming/${confId}`, 'res': thisConf, 'id': confId };

        thisConf = await DbManager.db.ref(`Conferences/Complete/${confId}`).once("value");
        if (thisConf.exists()) return { 'path': `Conferences/Complete/${confId}`, 'res': thisConf, 'id': confId };

        return null;
    }

    // TODO support more than one open conference at a time
    static async getRunningConferenceIdWithPIN(pin)
    {
        let matching = await DbManager.db.ref("Conferences/Live").orderByChild('PIN').equalTo(pin).once("value");
        if (matching.exists()) 
        {
            return {
                "state": "Live",
                "confId": Object.keys(matching.val())[0]
            }
        }

        matching = await DbManager.db.ref("Conferences/Upcoming").orderByChild('PIN').equalTo(pin).once("value");
        if (matching.exists()) 
        {
            return {
                "state": "Upcoming",
                "confId": Object.keys(matching.val())[0]
            }
        }

        // TODO support dialling in to previous conferences to get a summary?

        return null;
    }

    static async getGuestPasswordForConference(confId)
    {
        return await DbManager.db.ref(`Conferences/Live/${confId}/ShareLinkPwd`).once("value");
    }

    static async checkCanJoinConference(inboundNumber, confId, uuid)
    {
        let confRes = await DbManager.findConf(confId);

        if (!confRes) return false;

        const thisConf = confRes.res.val();

        // Check if we're being called by an existing contact
        var contact = await DbManager.db.ref(`Users/${thisConf.Owner}/Contacts/${inboundNumber}`).once("value");
        contact = contact.exists() ? contact.val() : null;

        var contactName = contact ? contact.Name : null;

        let allowed = true;
        let reason = 'allowed';

        if (thisConf.Participants && thisConf.Participants[inboundNumber] && thisConf.Participants[inboundNumber].Banned)
        {
            // don't allow in if banned
            allowed = false;
            reason = 'banned';
        }
        else if (!thisConf.Privacy.IsPublic)
        {
            // allow in if they're a participant
            if (!thisConf.Participants || !thisConf.Participants[inboundNumber])
            {
                allowed = thisConf.Privacy.AcceptContacts && contact != null;
                if (!allowed) reason = 'no-invite'
            }
        }

        if (allowed)
        {
            // check that there's capacity left in the call for the participant to be able to join.

            // get the current people in the call
            let activeParticipants = {};
            for (const phoneNumber in thisConf.Participants)
            {
                if (thisConf.Participants[phoneNumber].Status === "Active")
                    activeParticipants[phoneNumber] = {
                        ...thisConf.Participants[phoneNumber],
                    };
            }

            let numInCall = Object.keys(activeParticipants).length;

            // get the host's number, check if they're in the call/trying to join. Leave room for them if they're not
            const hostNum = (await DbManager.db.ref(`Users/${thisConf.Owner}/Phone`).once("value")).val();
            if (hostNum != inboundNumber && !hostNum in activeParticipants) numInCall++;

            allowed = numInCall < thisConf.MaxCapacity;
            if (!allowed) reason = 'capacity'
        }

        if (!allowed)
        {
            DbManager.db.ref(`${confRes.path}/Events`).push(
                {
                    "DateTime": Date.now(),
                    "Event": reason,
                    "Number": inboundNumber,
                    "ContactName": contactName
                });

            return { allowed, reason };
        }

        DbManager.db.ref(`Calls/${uuid}`).set({
            'PhoneNumber': inboundNumber,
            'Conference': confId,
            'Status': 'Active',
            'Talking': false,
            'CallUid': uuid,
            'Created': Date.now(),
            'Updated': Date.now()
        });

        let participantInfo = {
            "Status": 'Active'
        };

        if (contact) participantInfo.Name = contact.Name;

        DbManager.db.ref(`${confRes.path}/Participants/${inboundNumber}`).update(participantInfo);

        return { allowed, reason };
    }

    static async getTrunkByProxy(proxy)
    {
        let snapshot = await DbManager.db.ref(trunkStatusAddress).orderByChild('proxy').equalTo(proxy).limitToFirst(1).once("value");
        if(snapshot.exists())
        {
            let val = snapshot.val();
            // matching trunk is only child
            return val[Object.keys(val)[0]]
        } 
        return null;
    }

    static async getAppropriateTrunk(toCall, callJob = null)
    {
        let trunks = (await DbManager.db.ref(trunkStatusAddress).once("value")).val();

        if(callJob?.Trunk && trunks[callJob.Trunk]) return trunks[callJob.Trunk];

        let dialCode = langMapper.GetCountryDialCode(toCall);
        let suitableTrunks = [];

        // check for trunks specifically handling this country code first (they should have the lowest rate)
        for (const t in trunks) 
        {
            let thisTrunk = trunks[t];
            if (!thisTrunk.codes_cost_per_min) continue;

            if (thisTrunk.codes_cost_per_min[dialCode])
            {
                console.log(`${thisTrunk.gateway} suitable for dialcode ${dialCode}`)
                suitableTrunks.push(thisTrunk);
            }
        }

        if (suitableTrunks.length === 0)
        {
            console.log(`No trunks for ${dialCode} found, looking for international trunks`)
            // fall back on international trunks
            for (const t in trunks) 
            {
                if (trunks[t].handle_all_countries) suitableTrunks.push(trunks[t]);
            }
        }

        if (suitableTrunks.length > 0)
        {
            // use the least expensive suitable trunk
            suitableTrunks.sort((a, b) =>
            {
                return (a.codes_cost_per_min?.[dialCode] ? a.codes_cost_per_min[dialCode] : 999) -
                    (b.codes_cost_per_min?.[dialCode] ? b.codes_cost_per_min[dialCode] : 999)
            });
            console.log(`Using ${suitableTrunks[0].gateway}`)
            return suitableTrunks[0];
        }
        else
        {
            // no trunk found - default to first in the dict and cross fingers
            console.log(`Couldn't find a suitable trunk for calling ${toCall}, defaulting to ${trunks[Object.keys(trunks)[0]].gateway}`)
            return trunks[Object.keys(trunks)[0]];
        }
    }

    static async requestStartCall(callJob)
    {
        try 
        {
            if (DbManager.ongoingCalls[callJob.Number])
            {
                return console.debug("Already calling " + callJob.Number)
            }

            DbManager.ongoingCalls[callJob.Number] = true;

            let trunk = await DbManager.getAppropriateTrunk(callJob.Number, callJob);

            // get the callmanager to queue dialling this person
            // will call 'startCallCallback' when finished
            let addedToQueue = DbManager.callManager.queueCall(callJob.Number, callJob.ConferenceId, callJob.IsModerator, callJob.IsManualCall ?? true, trunk, callJob);

            if (!addedToQueue)
            {
                // this number is already in the queue - delete the job
                DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
            }

        }
        catch (error) 
        {
            DbManager.makeLog("ERROR", error, {
                "method": "requestStartCall",
                "number": callJob.Number,
                "dbId": callJob.id
            })
        }
    }

    static async queuedCallback(callJob)
    {
        console.log("Queued callback")
        let confRes = await DbManager.findConf(callJob.ConferenceId);
        await DbManager.db.ref(`${confRes.path}/Participants/${callJob.Number}`).update(
            {
                "Status": "Queued"
            }
        );
    }

    static async diallingCallback(callJob)
    {
        let confRes = await DbManager.findConf(callJob.ConferenceId);
        await DbManager.db.ref(`${confRes.path}/Participants/${callJob.Number}`).update(
            {
                "Status": "Dialling"
            }
        );
    }

    static initiatedCallCallback(callJob, callInfo)
    {
        // Keep track of this job's progress using its own UUID
        DbManager.db.ref(`FreeswitchJobs/${callInfo.job_uuid}`).set({
            'CallUuid': callInfo.uuid,
            'Datetime': Date.now()
        })

        DbManager.db.ref(`Calls/${callInfo.uuid}`).set({
            'PhoneNumber': callJob.Number,
            'Conference': callJob.ConferenceId,
            'Status': 'Dialling',
            'Talking': false,
            'CallUid': callInfo.uuid,
            'FreeswitchJobUuid': callInfo.job_uuid,
            'Created': Date.now(),
            'Updated': Date.now()
        });

        // transfer to jobs archive
        DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
        DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
    }

    static async retryCallCallback(recipient, conferenceName, attempts, delay)
    {
        let confRes = await DbManager.findConf(conferenceName);
        await DbManager.db.ref(`${confRes.path}/Participants/${recipient}`).update(
            {
                "Status": (delay ? "WAITING_RETRY " : "RETRYING ") + attempts
            }
        );

        let logEvent = {
            'DateTime': Date.now(),
            'Number': recipient,
            'Event': delay ? 'waiting_retry' : 'retrying',
            'Delay': delay,
            'Attempt': attempts
        }

        const contactName = await DbManager.GetContactName(confRes.res.val(), recipient)
        if (contactName)
        {
            logEvent.ContactName = contactName;
        }

        DbManager.db.ref(`${confRes.path}/Events`).push(logEvent);
    }

    static async failedCallCallback(callJob, err)
    {
        console.log("CALL FAILED:")
        callJob.Status = err;
        console.log(callJob)

        let confRes = await DbManager.findConf(callJob.ConferenceId);
        await DbManager.db.ref(`${confRes.path}/Participants/${callJob.Number}`).update(
            {
                "Status": "ERROR " + err
            }
        );

        let logEvent = {
            'DateTime': Date.now(),
            'Number': callJob.Number,
            'Event': 'failed',
            'Error': err
        };

        const contactName = await DbManager.GetContactName(confRes.res.val(), callJob.Number)
        if (contactName)
        {
            logEvent.ContactName = contactName;
        }
        DbManager.db.ref(`${confRes.path}/Events`).push(logEvent);

        DbManager.ongoingCalls[callJob.Number] = null;

        // transfer to jobs archive
        DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
        DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
    }

    static async requestHangupCall(callJob)
    {
        try 
        {
            console.debug("Disconnecting " + callJob.Uuid)

            // disconnect the call
            await DbManager.callManager.disconnectCall(callJob.Uuid);

            // update the call log
            DbManager.db.ref(`Calls/${callJob.Uuid}`).update({
                'Status': 'DISCONNECTED',
                'Talking': false,
                'Updated': Date.now()
            });

            // transfer to jobs archive
            DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
            DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
        }
        catch (error) 
        {
            DbManager.makeLog("ERROR", error, {
                "method": "requestStartCall",
                "number": callJob.Number,
                "dbId": callJob.id
            })
        }
    }

    static async requestMuteUnmuteCaller(callJob)
    {
        try 
        {
            console.debug(callJob.Task + " " + callJob.Uuid)

            // mute/unmute the member
            await DbManager.callManager.muteUnmuteCaller(callJob.ConferenceId, callJob.ConfMemberId, callJob.Task === 'mute')

            // transfer to jobs archive
            DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
            DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
        }
        catch (error) 
        {
            DbManager.makeLog("ERROR", error, {
                "method": "requestMuteUnmuteCaller",
                "confMemberId": callJob.ConfMemberId,
                "dbId": callJob.id
            })
        }
    }

    static async requestMuteUnmuteAll(callJob)
    {
        try 
        {
            console.debug(callJob.Task + " " + callJob.ConferenceId)

            // mute/unmute the member
            await DbManager.callManager.muteUnmuteAll(callJob.ConferenceId, callJob.Task === 'muteAll')

            // transfer to jobs archive
            DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
            DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
        }
        catch (error) 
        {
            console.log(error);
            DbManager.makeLog("ERROR", error, {
                "method": "requestMuteUnmuteAll",
                "conferenceId": callJob.ConferenceId,
                "dbId": callJob.id
            })
        }
    }

    static async requestEndConference(callJob)
    {
        try 
        {
            console.debug(callJob.Task + " " + callJob.ConferenceId)

            let conf = (await DbManager.findConf(callJob.ConferenceId)).res.val();
            let owner = (await DbManager.db.ref(`Users/${conf.Owner}`).once('value')).val()

            let audioFile = DbManager.soundsManager.getAudioPath('bd_conf_ended.wav', langMapper.MapPhoneToLanguage(owner.Phone));
            await DbManager.callManager.playAudioInConf(audioFile, callJob.ConferenceId);

            await new Promise(resolve => setTimeout(resolve, 8000));

            let res = await DbManager.callManager.disconnectAllFromConf(callJob.ConferenceId);

            if (res.body.startsWith('-ERR'))
            {
                console.log("ESL couldn't find conference, marking as finished manually");
                // something went wrong, e.g. conference might not have anyone in it

                let confRes = await DbManager.findConf(callJob.ConferenceId);

                conf = confRes.res.val();

                for (var num in conf.Participants)
                {
                    conf.Participants[num].Status = "Hangup";
                    DbManager.ongoingCalls[num] = null;
                }

                DbManager.db.ref(confRes.path).update({ "Active": false });

            }

            // transfer to jobs archive
            DbManager.db.ref(`${newJobsAddress}/${callJob.id}`).set(null);
            DbManager.db.ref(`${processedJobsAddress}/${callJob.id}`).set(callJob);
        }
        catch (error) 
        {
            console.error(error);
            DbManager.makeLog("ERROR", error, {
                "method": "requestEndConference",
                "conferenceId": callJob.ConferenceId ?? "No ID",
                "dbId": callJob.id ?? "No calljob id"
            })
        }
    }

    static async callEventHandler(event)
    {
        let call = {};
        let confRes = null;
        let confId = null;
        let isTalkEvent = false;
        if (event.uuid)
        {
            const snapshot = await DbManager.db.ref(`Calls/${event.uuid}`).once('value');
            call = snapshot.val();
            if (event.username == 1000) // this currently only supports one webrtc user...
            {
                console.log("host action through webrtc")
                call = {
                    PhoneNumber: event.username,
                }
                event.dialled = event.username;
            }
            else if (call)
            {
                confRes = await DbManager.findConf(call.Conference);
                confId = call.Conference;
            }
        }

        if (!confRes && event.confName)
        {
            confRes = await DbManager.findConf(event.confName);
            confId = event.confName;
        }

        let shouldLogEvent = true;
        let includeContactName = false;

        // Event for conference logs
        var logEvent = {
            "DateTime": Date.now(),
            "Event": event.eventName
        };

        try
        {
            switch (event.eventName)
            {
                case 'BACKGROUND_JOB':
                    var jobSnapShot = await DbManager.db.ref(`FreeswitchJobs/${event.jobUuid}`).once('value');
                    var callUuid = null;

                    if (jobSnapShot.exists())
                    {
                        callUuid = jobSnapShot.val().CallUuid;
                    }

                    if (callUuid)
                    {
                        DbManager.db.ref(`Calls/${callUuid}`).update({
                            'Status': event.body,
                            'StatusString': event.statusString
                        });

                        call = (await DbManager.db.ref(`Calls/${callUuid}`).once('value')).val();

                        if (call && !event.success)
                        {
                            DbManager.ongoingCalls[call.PhoneNumber] = null;

                            if (confRes)
                            {
                                DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update({
                                    "Status": "Hangup"
                                });
                            }
                        }
                    } else shouldLogEvent = false;
                    break;
                case 'CHANNEL_EXECUTE_COMPLETE':
                    if (confId)
                    {
                        if (['set', 'log', 'sleep', 'answer'].includes(event.currentApp)) return;

                        let timeout = (event.currentAppData === 'bdconferencing_outbound_consent' && event.ivrStatus === 'timeout') ||
                            (event.currentApp === 'play_and_get_digits' && event.ivrStatus === 'failure')

                        if (event.answerState === 'hangup' || timeout)
                        {
                            console.log("either hung up, or went to answermachine/didn't respond to IVR");
                            DbManager.ongoingCalls[event.dialled] = null;
                            DbManager.db.ref(`${confRes.path}/Participants/${event.dialled}`).update({
                                "Status": timeout ? "Timeout" : "Hangup"
                            });
                        }
                        else
                        {
                            if (event.currentApp == 'transfer' && event.currentAppData.includes(dialplan_consentThenJoin))
                            {
                                console.log('consenting')
                                DbManager.db.ref(`${confRes.path}/Participants/${event.dialled}`).update({
                                    "Status": 'Consenting'
                                });
                            }
                            else if (event.currentAppData && event.currentAppData.includes(dialplan_join))
                            {
                                console.log('active')
                                DbManager.db.ref(`${confRes.path}/Participants/${event.dialled}`).update({
                                    "Status": 'Active'
                                });
                            }
                        }
                    }
                    shouldLogEvent = false;
                    break;
                case 'RECORD_STOP':
                    // Support multiple recordings per participant during a conference in case they drop out
                    var stopped = Date.now();
                    let confRecDbLocalPath = `/Recordings/Participants/${event.dialled}/${stopped}`;
                    DbManager.db.ref(confRes.path + confRecDbLocalPath).update({
                        'Finished': stopped,
                        'LocalPath': event.path,
                        'RecordingDuration': event.milliseconds,
                        'Processing': true
                    });
                    DbManager.uploadRecording(event.path, confRecDbLocalPath, confId, stopped, event.dialled)
                    break;
                case 'start-talking':
                    isTalkEvent = true;
                    DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update({
                        "Speaking": true,
                        "LastStartedSpeech": Date.now()
                    });
                    break;
                case 'stop-talking':
                    isTalkEvent = true;
                    const participant = (confRes.res.val()).Participants[call.PhoneNumber];
                    if (participant.Speaking)
                    {
                        DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update({
                            "Speaking": false,
                            "TalkTime": DbManager.GetTalkTime(participant)
                        });
                    }
                    break;
                case 'mute-member':
                case 'unmute-member':
                    const currentState = (confRes.res.val()).Participants[call.PhoneNumber];
                    const newHandUp = currentState.HandUp ? event.eventName === 'mute-member' : false;

                    DbManager.db.ref(`Calls/${event.uuid}`).update({
                        "muted": event.eventName === 'mute-member'
                    });

                    let changedData = {
                        'Muted': event.eventName === 'mute-member',
                        'HandUp': newHandUp
                    }

                    if (event.eventName === 'mute-member' && currentState.Speaking)
                    {
                        // getting muted should count as stopping talking
                        changedData.Speaking = false;
                        changedData.TalkTime = DbManager.GetTalkTime(currentState);
                        isTalkEvent = true;
                    }

                    DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update(changedData);
                    includeContactName = true;
                    break;
                case 'mute-detect':
                    // TODO this doesn't have a stop event, so I think needs a timer to set as false?
                    console.log(event.uuid + " is trying to talk while muted")
                    break;
                case 'del-member':
                case 'kick-member':
                case 'add-member':
                    const isMod = await DbManager.checkIsModerator(confId, call.PhoneNumber);
                    const newData = {
                        "Status": event.eventName === 'add-member' ? "Active" : "Hangup",
                        "HandUp": false,
                        "Joined": true,
                        "Muted": !isMod,
                        "SessionData": {
                            "InCall": event.eventName === 'add-member',
                            'CallUuid': event.uuid,
                            'ConfMemberId': event.confMemberId
                        }
                    }

                    if (event.eventName !== 'add-member')
                    {
                        DbManager.ongoingCalls[call.PhoneNumber] = null;
                        const current = (confRes.res.val()).Participants[call.PhoneNumber];
                        if (current.Speaking)
                        {
                            isTalkEvent = true;
                            newData.Speaking = false;
                            newData.TalkTime = DbManager.GetTalkTime(current);
                        }
                        newData.LastLeft = Date.now();
                        newData.TotalCallTime = (current.TotalCallTime || 0) + newData.LastLeft - current.LastJoin;
                    }
                    else
                    {
                        newData.LastJoin = Date.now();
                    }

                    DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update(newData);
                    includeContactName = true;
                    break;
                case 'stop-recording':
                    var stopped = Date.now();
                    let recDbLocalPath = `/Recordings/Conference/${stopped}`;
                    DbManager.db.ref(confRes.path + recDbLocalPath).update({
                        'Finished': stopped,
                        'LocalPath': event.path,
                        'RecordingDuration': event.milliseconds,
                        'Processing': true
                    });
                    DbManager.uploadRecording(event.path, recDbLocalPath, confId, stopped)
                    break;
                case 'conference-create':
                    DbManager.ongoingConfs[confId] = true;
                    DbManager.db.ref(`${confRes.path}`).update({
                        'Active': true,
                        'Status': 1,
                        'Started': Date.now()
                    });
                    break;
                case 'conference-destroy':
                    DbManager.ongoingConfs[confId] = null;
                    DbManager.db.ref(`${confRes.path}`).update({
                        "Active": false
                    });
                    break;
                case 'play-file-done':
                    // don't care about this at the moment.
                    break;
                case 'floor-change':
                    DbManager.db.ref(`${confRes.path}`).update({
                        "FloorMember": event.newFloorMemberId
                    });
                    break;
                case 'rejected-caller':
                    includeContactName = true;
                    DbManager.ongoingCalls[call.phoneNumber] = null;
                    break;
                case 'dtmf':
                    console.log(`${call.PhoneNumber} pressed ${event.pressed}`)
                    switch (event.pressed)
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                            let logDetails = await DbManager.TryVote(confRes, call.PhoneNumber, event.pressed);
                            if (logDetails) 
                            {
                                if (logDetails.Anon)
                                {
                                    logEvent.Event = logDetails.Revote ? 'anon_revote' : 'anon_vote';
                                }
                                else
                                {
                                    logEvent.Event = logDetails.Revote ? 'revote' : 'vote';
                                }
                                logEvent.Option = event.pressed;
                                includeContactName = true;
                            }
                            break;
                        case "8":
                            let handUp = await DbManager.HandUpDown(confRes, call.PhoneNumber)
                            logEvent.Event = handUp ? 'hand_up' : 'hand_down';
                            includeContactName = true;
                            break;
                        case "0":
                            // Participants can press 0 to mute themselves, but can't unmute
                            // Moderators are able to unmute themselves
                            const conf = confRes.res.val();
                            const participant = conf.Participants[call.PhoneNumber];
                            const shouldMute = participant.Moderator ? participant.Muted == false : true;

                            if (participant.LastMuteInputEvent && (Date.now() - participant.LastMuteInputEvent < 1000))
                            {
                                // inputs too close together
                                console.log('Mute/unmute input repeated, ignoring')
                                return;
                            }
                            else
                            {
                                DbManager.db.ref(`${confRes.path}/Participants/${call.PhoneNumber}`).update({
                                    'LastMuteInputEvent': Date.now()
                                })
                            }
                            await DbManager.callManager.muteUnmuteCaller(confId, participant.SessionData.ConfMemberId, shouldMute)
                            break;
                        default:
                            console.log('Unused DTMF pressed by ' + call.PhoneNumber);
                            break;
                    }
                    break;
                default:
                    console.log("dbmanager unhandled event: " + event.eventName);
            }

            if (!shouldLogEvent) return;

            if (call)
            {
                logEvent.Number = call.PhoneNumber;

                if (includeContactName)
                {
                    const contactName = await DbManager.GetContactName(confRes.res.val(), call.PhoneNumber)
                    if (contactName)
                    {
                        logEvent.ContactName = contactName;
                    }
                }
            }

            if (confRes)
            {
                if (isTalkEvent)
                {
                    // all events which get used to track time talked
                    // includes start/stop talking, mute, leave etc
                    DbManager.db.ref(`${confRes.path}/TalkEvents`).push(logEvent);
                }

                if (logEvent.Event != 'start-talking' && logEvent.Event != 'stop-talking')
                {
                    // everything other than start/stop talking events, which can be spammy
                    if (!logEvent.Number)
                    {
                        // can be undefined here, which firebase rtdb doesn't like
                        delete logEvent.Number
                    }
                    DbManager.db.ref(`${confRes.path}/Events`).push(logEvent);
                }
            }

        }
        catch (error) 
        {
            console.error(error);
            DbManager.makeLog("ERROR", error, error)
        }

    }

    static async GetContactName(conf, phoneNumber)
    {
        var contactSnapshot = await DbManager.db.ref(`Users/${conf.Owner}/Contacts/${phoneNumber}`).once('value')
        if (contactSnapshot.exists())
        {
            return contactSnapshot.val().Name;
        }
        return null;
    }

    static async TryVote(confRes, phoneNumber, vote)
    {
        let conf = confRes.res.val();
        if (!conf.Polls) return null;

        let pollIds = Object.keys(conf.Polls);
        let lastPoll = conf.Polls[pollIds[pollIds.length - 1]];

        // options are index 0, but DTMF votes start from 1 for ease of use
        vote -= 1;

        if (!lastPoll.Open) return null;
        if (!lastPoll.Options[vote]) return null;

        let revote = lastPoll.Results && lastPoll.Results[phoneNumber];

        await DbManager.db.ref(`${confRes.path}/Polls/${pollIds[pollIds.length - 1]}/Results/${phoneNumber}`).update({
            'Vote': vote,
            'Time': Date.now()
        })

        let memberId = conf.Participants[phoneNumber].SessionData.ConfMemberId;
        let audioFile = DbManager.soundsManager.getAudioPath('thanks_for_voting.wav', langMapper.MapPhoneToLanguage(phoneNumber));
        await DbManager.callManager.playAudioInConf(audioFile, confRes.id, memberId);

        return {
            'Anon': lastPoll.Anonymous,
            'Revote': revote,
            'Option': lastPoll.Options[vote].Title
        };
    }

    static async HandUpDown(confRes, phoneNumber)
    {
        const participant = (await DbManager.db.ref(`${confRes.path}/Participants/${phoneNumber}`).once("value")).val();

        const handUp = !participant.HandUp;

        if (participant.LastHandInputEvent && (Date.now() - participant.LastHandInputEvent < 1000))
        {
            // inputs too close together
            console.log('Hand raise input repeated, ignoring')
            return;
        }

        await DbManager.db.ref(`${confRes.path}/Participants/${phoneNumber}`).update({
            'HandUp': handUp,
            'LastHandInputEvent': Date.now()
        })

        const conf = confRes.res.val();
        const memberId = conf.Participants[phoneNumber].SessionData.ConfMemberId;
        const audioFile = DbManager.soundsManager.getAudioPath(handUp ? 'raised_hand.wav' : 'lowered_hand.wav', langMapper.MapPhoneToLanguage(phoneNumber));
        await DbManager.callManager.playAudioInConf(audioFile, confRes.id, memberId);

        return handUp;
    }

    static GetTalkTime(participant)
    {
        // in case stop gets sent before start somehow
        if (!participant.LastStartedSpeech) participant.LastStartedSpeech = Date.now();

        var talkTime = Date.now() - participant.LastStartedSpeech;
        if (participant.TalkTime) talkTime += participant.TalkTime;
        return talkTime;
    }

    static makeLog(type, message, data)
    {
        DbManager.db.ref(logsRoot).push({
            "dateTime": Date.now(),
            "type": type ?? "No type",
            "message": message ?? "No message",
            "data": data ?? "No data"
        })
    }

    static async uploadRecording(path, dbLocalPath, confName, stoppedAt, participant)
    {
        try
        {
            let confRes = await DbManager.findConf(confName);
            const thisConf = confRes.res.val();

            // Upload wav file, FB Functions will convert to MP3 and store ref in db
            var destination = participant ?
                `${uploadsRoot}/${thisConf.Owner}/${confName}/${participant}/${stoppedAt}.wav` :
                `${uploadsRoot}/${thisConf.Owner}/${confName}/conference/${stoppedAt}.wav`

            await DbManager.storage.bucket().upload(path, {
                destination: destination,
                metadata: {
                    contentType: 'audio/wav',
                    cacheControl: 'public, max-age=31536000'
                }
            })

            console.log("Upload successful", destination)

            // Make sure the local wav is deleted
            fs.unlink(path, (err => err ? console.log('Delete err: ' + err) : console.log('Deleted ' + path)));

            // conference might have moved in the db while we uploaded this file, get up-to-date location
            confRes = await DbManager.findConf(confName);

            DbManager.db.ref(confRes.path + dbLocalPath).update({
                'Upload': destination
            });

        }
        catch (error) 
        {
            console.log("Error in uploadRecording! " + error)
            DbManager.makeLog("ERROR", error, error)
        }
    }
}

module.exports = DbManager;