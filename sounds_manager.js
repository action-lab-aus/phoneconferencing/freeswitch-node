const soundsDetails = require('./audioFilesList.json');
const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');

class SoundsManager 
{
    constructor(localPath)
    {
        this.localPath = localPath;
    }

    langFolders = {};

    async prepareFiles(bucket, remoteParentDir, localLangFolders)
    {
        this.langFolders = localLangFolders;

        for(var language in localLangFolders)
        {
            var samples = ['8000', '16000', '32000'];

            for(var i = 0; i < samples.length; i++)
            {
                var sample = samples[i];
                var thisFolder = `${localLangFolders[language]}/bdconf/${sample}`;

                console.log(`Checking ${language} audio cache at ${thisFolder}`)

                await this.createDirectories(thisFolder)

                var existingFiles = await fsPromises.readdir(thisFolder);
                var promises = [];

                for(var filename in soundsDetails.lexicon)
                {
                    if(existingFiles.includes(filename))
                    {
                        continue;
                    }

                    try
                    {
                        promises.push(bucket.file(`${remoteParentDir}/${language}/${sample}/${filename}`).download( {
                            destination: `${thisFolder}/${filename}`
                        }));
                        console.log(`Downloading ${filename}`);   
                    }
                    catch (error)
                    {
                        // file probably doesn't exist in cloud
                        console.log(error.message)
                        fsPromises.unlink(`${thisFolder}/${filename}`);
                    }
                }
                try 
                {
                    await Promise.all(promises);

                    console.log(`All ${language} ${sample} files ready`)
                }
                catch (error) 
                {
                    console.error(`WARNING! Download error! Check that all ${language} ${sample} files are available for download!`, error.message)
                }
            }
        }
    }

    createDirectories(pathname) {
        return new Promise((resolve, reject) =>
        {
            fs.mkdir(path.resolve(__dirname, pathname), { recursive: true }, err => 
            {
                if (err) 
                {
                    if (err.code == 'EEXIST')
                    {
                        resolve();
                    }
                    console.error(err);
                    reject();
                } 
                else 
                {
                    resolve();
                }
            });
        });
    }

    getAudioPath(filename, lang, rate = '16000')
    {
        return `${this.langFolders[lang]}/bdconf/${rate}/${filename}`
    }
    
}

module.exports = SoundsManager;