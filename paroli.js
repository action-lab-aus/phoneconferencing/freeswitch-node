const SoundsManager = require('./sounds_manager.js');
const CallManager = require('./call_manager.js');
const DbManager = require('./db_manager.js');
const admin = require('firebase-admin');
const fs = require('fs');
const path = require("path");
const serviceAccount = JSON.parse(fs.readFileSync(path.resolve(__dirname,'../keys/firebase_service_acc_private_key.json'), 'utf8')); // !! KEYS FOLDER IS OUTSIDE OF REPO !!
const langSettings = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../keys/languages.json'), 'utf8')); // !! KEYS FOLDER IS OUTSIDE OF REPO !!
const fbConfig = JSON.parse(fs.readFileSync(path.resolve(__dirname,'../keys/firebase_config.json'), 'utf8')); // !! KEYS FOLDER IS OUTSIDE OF REPO !!
const process = require('process');
const { exec } = require("child_process");
const xml2js = require('xml2js');
const express = require('express');

// Check if we're running as a user in the freeswitch group
function runUserGroupCheck()
{
  exec("id -g freeswitch", (error, stdout, stderr) => {
    if (error) {
        console.error(`error getting freeswitch group info: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr getting freeswitch group info: ${stderr}`);
        return;
    }

    if (process.getgid) {                  
      var gid = process.getgid();
      if(gid == stdout)
      {
        console.log("Running as the freeswitch group, gid", gid)
      }
      else
      {
        console.warn("Not being run in the 'freeswitch' user group. May encounter permission issues with recordings!")
      }
    }
    else
    {
      console.warn("OS doesn't support programmatic checking of usergroup. Make sure this is being run as a member of the 'freeswitch' group!")
    }
  });
}

async function initialiseFreeswitchApp()
{
  // Init Firebase connection
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: fbConfig.databaseURL,
    storageBucket: fbConfig.storageBucket
  });
  const db = admin.database();
  const storage = admin.storage();

  let audioFileLocations = {};

  langSettings.supported.forEach(lang => {
    audioFileLocations[lang] = `/usr/local/freeswitch/sounds/${lang}`;
  });

  const soundsManager = new SoundsManager('./downloadedAudio');
  await soundsManager.prepareFiles(storage.bucket(), 'audio', audioFileLocations);

  DbManager.callManager = new CallManager(DbManager);
  DbManager.soundsManager = soundsManager;
  DbManager.db = db;
  DbManager.storage = storage;
  DbManager.startMonitoring();
}

runUserGroupCheck();
initialiseFreeswitchApp();



// WEBSERVER
// Used for dynamic freeswitch user creation
// Create HTTP server and listen on port 8080 for requests from FS
const app = express();
app.use(
  express.urlencoded({
    extended: true
  })
);
app.use(express.json());

app.listen(8080, () => {
  console.log("Node server listening")
})