const fs = require('fs');
const path = require("path");
const trunkDetails = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../keys/siptrunks.json'), 'utf8'));
const langMapper = require('./language_mapper.js');
const esl = require('modesl');

// Dialplan destinations
const dialplan_setLanguage = 8675309;
const dialplan_reject = 69421;
const dialplan_reject_capacity = 69422;
const dialplan_join = 69420;
const dialplan_consentThenJoin = 8675310;
const dialplan_hostJoin = 9786421;
const dialplan_inboundCallback = 4066;
const totalRetries = 3;

const Event = {
    Connection: {
        READY: 'esl::ready',
        CLOSED: 'esl::end',
        ERROR: 'error',
    },
    RECEIVED: 'esl::event::*::*',
    DialResults: {
        OK: '+OK',
        REJECTED: 'CALL_REJECTED',
        NO_USER: 'USER_NOT_REGISTERED',
        BUSY: 'USER_BUSY',
        NO_ANSWER: 'NO_ANSWER',
        NO_RESPONSE: 'NO_USER_RESPONSE'
    }
};

const subbedEvents = ['RECORD_STOP', 'CUSTOM conference::maintenance', 'CHANNEL_EXECUTE_COMPLETE', 'CHANNEL_DESTROY', 'BACKGROUND_JOB', 'DTMF'];
const rateLimit = 1100;

// Created using https://medium.com/makingtuenti/developing-applications-for-freeswitch-fccbe75ada81
class CallManager
{
    constructor(dbManager)
    {
        this.eventHandler = dbManager.callEventHandler;
        this.queuedCallback = dbManager.queuedCallback;
        this.diallingCallback = dbManager.diallingCallback;
        this.initiatedCallCallback = dbManager.initiatedCallCallback;
        this.retryCallCallback = dbManager.retryCallCallback;
        this.failedCallCallback = dbManager.failedCallCallback;
        this.getAppropriateTrunk = dbManager.getAppropriateTrunk;
        this.getTrunkByProxy = dbManager.getTrunkByProxy;
        this.callQueue = [];
        this.lastDialled = Date.now() - rateLimit;
        this.timerId = null;
        this.processedEvents = [];

        // initialise inbound ESL connection to start listening for events
        this.eslExecute('log NOTICE Starting inbound ESL')

        // handle incoming calls with an outbound ESL connection
        this.eslServer = new esl.Server({ port: 8040, myevents: true }, () =>
        {
            console.log('esl outbound server is up');
        });
        this.eslServer.on('error', (error) => {
            console.log("ESL ERROR EVENT", error)
        });
        this.eslServer.on('connection::ready', async (conn, id) =>
        {
            try
            {
                let info = conn.getInfo();
                let callerNum = info.getHeader('Caller-Caller-ID-Number');

                if (!info.getHeader('variable_bdconference_callback'))
                {
                    // this is a new inbound call
                    let trunk = await this.getAppropriateTrunk(callerNum);

                    if (!trunk) trunk = trunkDetails[0];
                    else console.log('Call to trunk: ' + trunk.gateway);

                    // play audio message, drop call and dial them instead, to avoid participant being charged
                    let lang = langMapper.MapPhoneToLanguage(callerNum);
                    let internationalCode = langMapper.GetCountryDialCode(callerNum);

                    if(!internationalCode)
                    {
                        // if we failed to get international dialcode from the phone number, this is probably a local call
                        // use the connected proxy address to get the correct trunk, then use its dialcode
                        trunk = await this.getTrunkByProxy(info.getHeader('variable_sip_from_host'));
                        internationalCode = langMapper.GetCountryDialCode(trunk.caller_number);
                    }

                    let numToCall = this.getNumberToCall(trunk, internationalCode, callerNum)
                    let callLocal = internationalCode === null || numToCall !== callerNum;
                    let originateArgs = this.getOriginateArgs(lang, lang, trunk, internationalCode, callLocal,
                        `bdconference_nextdestination=${dialplan_inboundCallback},bdconference_callback=true`);

                    conn.on('error', async (error) => {
                        // user probably just hung up before the message finished playing
                        // this needs to be caught otherwise the ESL library crashes
                        console.log("ESL ERROR EVENT", error)
                    });

                    conn.execute('playback', 'silence_stream://1500', () => {
                        conn.execute('playback', `file_string://${lang}/bdconf/bd_conf_callback.wav`, async () => {
                            conn.execute('hangup', 'USER_BUSY');
                            console.log("Dropped inbound call from " + callerNum + ', waiting to call back');
                            await this.delay(20000);
                            console.log("Returning call to " + callerNum)
                            return this.eslExecute(`originate {${originateArgs}}sofia/gateway/${trunk.gateway}/${numToCall} ${dialplan_setLanguage}`);
                        });
                    });

                    return;
                }

                // This is an outbound call, returning an automatically dropped inbound call (above)
                // The callee has already entered a PIN of the conference they want to join

                callerNum = this.standardiseToInternationalFormat(callerNum, info)

                let uuid = info.getHeader('variable_uuid')
                let pin = info.getHeader('variable_bdconf_dtmf')
                let pinRes = await dbManager.getRunningConferenceIdWithPIN(pin);

                console.log("Returned call to " + callerNum);

                if (!pinRes)
                {
                    // unfortunately there's no available conference with that pin
                    conn.execute('playback', 'file_string://${default_language}/bdconf/bd_conf_enter_pin_wrong.wav!${default_language}/bdconf/bd_conf_have_a_nice_day.wav');
                    console.log(callerNum + " entered invalid pin: " + pin)
                    return conn.disconnect();
                }
                else if (pinRes.state != "Live")
                {
                    // conference hasn't started yet, please come back later
                    conn.execute('playback', 'file_string://${default_language}/bdconf/bd_conf_enter_pin_early.wav!${default_language}/bdconf/bd_conf_have_a_nice_day.wav');
                    console.log(`${callerNum} entered pin (${pin})of upcoming conf ${pinRes.confId}`)
                    return conn.disconnect();
                }
                else
                {
                    // join the conference
                    let guessedLang = langMapper.MapPhoneToLanguage(callerNum);
                    const canJoinRes = await dbManager.checkCanJoinConference(callerNum, pinRes.confId, uuid);

                    if (pinRes.confId && canJoinRes.allowed)
                    {
                        // caller is allowed in
                        console.log(`${callerNum} ${uuid} is able to join the conf ${pinRes.confId}`);
                        conn.execute('playback', '${default_language}/bdconf/bd_conf_enter_pin_correct.wav');
                        let isMod = await dbManager.checkIsModerator(pinRes.confId, callerNum);

                        conn.execute('multiset', `bdconference_name=${pinRes.confId} bdconference_lang=${guessedLang} bdconference_nextdestination=${dialplan_join} bdconference_admin=${isMod}`);        
                    }
                    else if(pinRes.confId && canJoinRes.reason === 'capacity')
                    {
                        // not enough space in this call
                        conn.execute('multiset', `bdconference_nextdestination=${dialplan_reject_capacity} bdconference_lang=${guessedLang}`);
                        console.log("Transferred to language and then rejection. Reason:" + canJoinRes.reason)
                    }
                    else
                    {
                        // no conferences running, banned, or not invited (handled in xml)
                        conn.execute('multiset', `bdconference_nextdestination=${dialplan_reject} bdconference_lang=${guessedLang} bdconference_confExists=${pinRes.confId != null}`);
                        console.log("Transferred to language and then rejection. Reason:" + pinRes.confId ? canJoinRes.reason : "PIN doesn't match")
                    }

                    conn.execute('transfer', dialplan_setLanguage);   
                    return conn.disconnect();
                    
                }
            } 
            catch (error) 
            {
                console.log("CONNECTION READY ERROR")
                console.error(error)    
                conn.disconnect();
            }
            
        });
    }

    eslConnect(retries = 0)
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                if (!this.eslConn)
                {
                    console.log("Connecting...")
                    this.eslConn = new esl.Connection('::1', '8021', 'ClueCon');
                    this.eslConn.on(Event.Connection.ERROR, async (err) =>
                    {
                        // Error connecting to FreeSWITCH!
                        console.error(err);
                        this.eslConn = null;
                        if (retries > 10)
                        {
                            reject('Connection error');
                        }
                        else
                        {
                            retries++;
                            console.log("waiting to retry...");
                            await this.delay(1000 * retries)
                            console.log("retrying... " + retries)
                            return this.eslConnect(retries);
                        }

                    });
                    this.eslConn.on(Event.Connection.CLOSED, () =>
                    {
                        // Connection to FreeSWITCH closed!
                        this.eslConn = null;
                        reject('Connection closed');
                    });
                    this.eslConn.on(Event.Connection.READY, () =>
                    {
                        // Connection to FreeSWITCH established!
                        console.log("connection established")
                        this.eslConn.events('json', subbedEvents);
                        resolve(this.eslConn);
                    });
                    this.eslConn.on(Event.RECEIVED, (event) =>
                    {
                        this.processEvent(event)
                    });
                }
                else
                {
                    resolve(this.eslConn);
                }
            }
            catch (error) 
            {
                console.log(error)
                reject(error)
            }
        });
    }

    delay(time)
    {
        return new Promise(resolve => setTimeout(resolve, time));
    }

    getNumberToCall(trunk, intCode, callerNum)
    {
        return trunk.removeIntCode && intCode !== null? 
            callerNum.replace(intCode, trunk.addZeroToLocal ? '0' : '') 
            : callerNum;
    }

    // if necessary, remove leading 0 (if there) and replace with stored international code
    standardiseToInternationalFormat(callerNum, callData)
    {
        if(callData.getHeader('variable_bdconference_local') === 'true')
        {
            let original = callerNum;
            callerNum = callData.getHeader('variable_bdconference_intcode') + (callData.getHeader('variable_bdconference_addZeroToLocal') === 'true' ? callerNum.substring(1) : callerNum);
            console.log(`converted ${original} to ${callerNum}`);
        }

        return callerNum;
    }

    eslExecute(command)
    {
        return new Promise((resolve, reject) => 
        {
            this.eslConnect()
                .then(connection =>
                {
                    connection.bgapi(command, response =>
                    {
                        resolve(response);
                    });
                })
                .catch(error =>
                {
                    if (error && error.code && error.code === 'ERR_STREAM_DESTROYED')
                    {
                        throw error;
                    }
                    else
                    {
                        reject(error);
                    }
                });
        });
    }

    getTrunkDetails()
    {
        return trunkDetails;
    }

    async processEvent(event)
    {
        if (!event.getType()) return; //ain't no-one got time for that

        var eventKey = event.getHeader('Event-Sequence');

        // keep an array of the 40 most recent received events
        // don't want to store them all as we're going to receive a lot of them,
        // and only care about the most recent ones anyway (to avoid duplicates)
        if (this.processedEvents.includes(eventKey)) return; // duplicate
        this.processedEvents.push(eventKey)
        if (this.processedEvents.length > 40)
        {
            this.processedEvents.shift();
        }

        var toReturn = {
            'eventName': event.getHeader('Event-Name'),
            'body': event.getBody(),
            'confName': event.getHeader('variable_bdconference_name'),
            'statusString': event.getBody()
        };

        switch (toReturn.eventName)
        {
            case 'BACKGROUND_JOB':
                toReturn.jobCommand = event.getHeader('Job-Command');
                if (toReturn.jobCommand === 'create_uuid') return; // not interested

                toReturn.jobUuid = event.getHeader('Job-UUID')

                // 'originate' creates an outbound call
                if (toReturn.jobCommand === 'originate')
                {
                    // TODO strings should be in resource files for translation
                    if (toReturn.body.startsWith(Event.DialResults.OK))
                    {
                        toReturn.statusString = "Call made successfully";
                        toReturn.success = true;
                    }
                    else
                    {
                        toReturn.success = false;
                        if (toReturn.body.includes(Event.DialResults.REJECTED))
                        {
                            toReturn.statusString = "Call rejected";
                            toReturn.body = Event.DialResults.REJECTED;
                        }
                        else if (toReturn.body.includes(Event.DialResults.NO_USER))
                        {
                            toReturn.statusString = "Called user not registered";
                            toReturn.body = Event.DialResults.NO_USER;
                        }
                        else if (toReturn.body.includes(Event.DialResults.BUSY))
                        {
                            toReturn.statusString = "Called user was engaged";
                            toReturn.body = Event.DialResults.BUSY;
                        }
                        else if (toReturn.body.includes(Event.DialResults.NO_ANSWER))
                        {
                            toReturn.statusString = "User didn't answer";
                            toReturn.body = Event.DialResults.NO_ANSWER;
                        }
                        else if (toReturn.body.includes(Event.DialResults.NO_RESPONSE))
                        {
                            toReturn.statusString = "User's device did not respond";
                            toReturn.body = Event.DialResults.NO_RESPONSE;
                        }
                    }
                }
                else if (toReturn.jobCommand === 'uuid_kill')
                {
                    if (toReturn.body.startsWith(Event.DialResults.OK))
                    {
                        toReturn.statusString = "Call terminated";
                    }
                }
                break;

            case 'CHANNEL_EXECUTE_COMPLETE':
                toReturn.direction = event.getHeader('variable_direction');
                toReturn.dialled = toReturn.direction == 'inbound' ? event.getHeader('variable_sip_from_user') : event.getHeader('variable_sip_to_user');
                toReturn.currentApp = event.getHeader('Application');
                toReturn.currentAppData = event.getHeader('Application-Data');
                toReturn.ivrStatus = event.getHeader('variable_ivr_menu_status') ? event.getHeader('variable_ivr_menu_status') : event.getHeader('variable_read_result');
                toReturn.answerState = event.getHeader('Answer-State');
                toReturn.uuid = event.getHeader('Unique-ID');
                //console.log(event.serialize())
                break;

            case 'RECORD_STOP':
                toReturn.path = event.getHeader('Record-File-Path');
                toReturn.milliseconds = event.getHeader('variable_record_ms');
                toReturn.direction = event.getHeader('variable_direction');
                toReturn.dialled = toReturn.direction == 'inbound' ? event.getHeader('variable_sip_from_user') : event.getHeader('variable_sip_to_user');
                break;

            case 'CHANNEL_DESTROY':
                console.log(event);
                break;

            // CUSTOM includes all conference related events
            case 'CUSTOM':
                if (event.getHeader('Event-Subclass') === 'conference::maintenance')
                {
                    // For all action strings see 'Table: Conference Events' 
                    // on https://freeswitch.org/confluence/display/FREESWITCH/mod_conference
                    toReturn.eventName = event.getHeader('Action');
                    toReturn.uuid = event.getHeader('Unique-ID') ? event.getHeader('Unique-ID') : null;
                    toReturn.username = event.getHeader('Caller-Username');
                    toReturn.hasFloor = event.getHeader('Floor') === 'true';
                    toReturn.confName = event.getHeader('Conference-Name');
                    toReturn.confMemberId = event.getHeader('Member-ID');
                    toReturn.file = event.getHeader('File');
                    toReturn.newFloorMemberId = event.getHeader('New-ID')
                    toReturn.path = event.getHeader('Path');
                    toReturn.pressed = event.getHeader('DTMF-Key'); // conference controls absorb dtmf events and push them through here
                    toReturn.milliseconds = event.getHeader('Milliseconds-Elapsed');
                    //console.log("Custom event: " + toReturn.eventName);
                    //console.log(event.serialize()) // uncomment to spam entire event data to console
                    break;
                }
            // --------!! flows into default if not a conference event !!-------
            default:
                console.log("call manager Unhandled event: " + toReturn.eventName);
                toReturn.raw = event;
        }

        if(toReturn.dialled)
        {
            toReturn.dialled = this.standardiseToInternationalFormat(toReturn.dialled, event);
        } 

        if (this.eventHandler)
        {
            // hand over to the event handler callback
            // for db logging + gui updates
            toReturn.time = Date.now();
            this.eventHandler(toReturn);
        }
    }

    calculateTimeRemaining()
    {
        const timeDelta = Date.now() - this.lastDialled;
        return Math.max(rateLimit - timeDelta, 0);
    }

    queueCall(recipient, conferenceName, isMod, manualCall, trunk, callbackData)
    {
        // check for dupes
        for (let i = 0; i < this.callQueue.length; i++)
        {
            if (this.callQueue[i].recipient == recipient)
            {
                console.log('duplicate caller, not adding to queue');
                return false;
            }
        }

        let delay = this.calculateTimeRemaining();

        if (delay == 0 && this.timerId == null)
        {
            console.log("Can call straight away")
            this.diallingCallback(callbackData);
            this.makeCall(recipient, conferenceName, totalRetries, isMod, trunk, manualCall)
                .then(callInfo =>
                {
                    this.initiatedCallCallback(callbackData, callInfo);
                })
                .catch(err =>
                {
                    this.failedCallCallback(callbackData, err);
                });
            return true;
        }

        let callData = {
            'recipient': recipient,
            'conferenceName': conferenceName,
            'isMod': isMod,
            'trunk' : trunk,
            'callbackData': callbackData
        };

        if (isMod)
        {
            // mods go to front of queue (after any waiting mods).
            // Default to end, in case the queue is empty or only contains mods
            let firstAvailableSpot = this.callQueue.length;

            for (let i = 0; i < this.callQueue.length; i++)
            {
                if (!this.callQueue[i].isMod)
                {
                    firstAvailableSpot = i;
                    console.log('inserting mod at position ' + i);
                    break;
                }
            }

            this.callQueue.splice(firstAvailableSpot, 0, callData);
        }
        else
        {
            // everyone else joins the back of the queue
            this.callQueue.push(callData);
        }

        console.log('call queued for ' + delay)
        this.queuedCallback(callbackData);

        if (this.timerId == null)
        {
            this.timerId = setTimeout(this.popQueue.bind(this), this.calculateTimeRemaining());
        }

        return true;
    }

    async popQueue()
    {
        if (this.callQueue.length == 0) return;

        console.log('queue popped')
        const thisCall = this.callQueue.shift();
        console.log(thisCall)

        this.diallingCallback(thisCall.callbackData);

        this.makeCall(thisCall.recipient, thisCall.conferenceName, totalRetries, thisCall.isMod, thisCall.trunk)
            .then(callInfo =>
            {
                this.initiatedCallCallback(thisCall.callbackData, callInfo);
            })
            .catch(err =>
            {
                this.failedCallCallback(thisCall.callbackData, err);
            });

        this.timerId = this.callQueue.length > 0 ?
            setTimeout(this.popQueue.bind(this), this.calculateTimeRemaining()) :
            null;
    }

    getOriginateArgs(lang, defaultLang, trunk, internationalCode, isLocalCall, customArgs = null)
    {
        let toRet = `bdconference_lang=${lang},default_language=${defaultLang},bdconference_intcode=${internationalCode},`+
        `bdconference_local=${isLocalCall},bdconference_addZeroToLocal=${trunk.addZeroToLocal},`+
        `origination_caller_id_number=${trunk.caller_number},ignore_early_media=true,execute_on_answer=start_dtmf`;
        
        if(customArgs) toRet = toRet.concat(',',customArgs);
        
        return toRet;
    }

    async makeCall(recipient, conferenceName, retries, isMod, trunk, manualCall = true)
    {
        retries--;
        try 
        {
            this.lastDialled = Date.now();
            var lang = langMapper.MapPhoneToLanguage(recipient);
            var uuid_res = await this.eslExecute('create_uuid')
            var uuid = uuid_res.getBody();

            let internationalCode = langMapper.GetCountryDialCode(recipient);
            let numToCall = this.getNumberToCall(trunk, internationalCode, recipient)

            let originateArgs = this.getOriginateArgs(lang, lang, trunk, internationalCode, numToCall !== recipient, 
                `bdconference_manual=${manualCall},bdconference_nextdestination=${isMod ? dialplan_hostJoin : dialplan_consentThenJoin},bdconference_name=${conferenceName},origination_uuid=${uuid}`);

            // initialise call using our uuid so we can track it
            var originate_res = await this.eslExecute(`originate {${originateArgs}}sofia/gateway/${trunk.gateway}/${numToCall} ${dialplan_setLanguage}`);

            if (originate_res && originate_res.body.includes('-ERR'))
            {
                throw originate_res.body;
            }
            else
            {
                return { 'uuid': uuid, 'job_uuid': originate_res.getHeader("Job-UUID") };
            }
        }
        catch (error) 
        {
            error = error.replace('-ERR', '').trim();

            console.log(`${recipient} call error: ${error}`);
            if (error.includes(Event.DialResults.BUSY) || error.includes(Event.DialResults.REJECTED))
            {
                // if the user probably doesn't want to receive the call right now, don't retry
                console.log('Not going to retry')
                throw error;
            }
            else if (retries > 0)
            {
                const attempts = totalRetries - retries;
                // wait an increasing amount before retrying
                const timeToWait = 4000 * (attempts * attempts);
                this.retryCallCallback(recipient, conferenceName, attempts + 1, timeToWait);
                console.log('retry waiting ' + timeToWait)
                await new Promise(r => setTimeout(r, timeToWait));
                this.retryCallCallback(recipient, conferenceName, attempts + 1, null);
                return this.makeCall(recipient, conferenceName, retries, isMod, trunk)
            }
            else throw error;
        }
    }

    async playAudioInConf(audioPath, confName, memberId = null)
    {
        let command = `conference ${confName} play ${audioPath}`;
        if(memberId) command != ` ${memberId}`
        console.log(command)
        return await this.eslExecute(command);
    }

    async disconnectCall(uuid)
    {
        return await this.eslExecute(`uuid_kill ${uuid}`);
    }

    async disconnectAllFromConf(confName)
    {
        return await this.eslExecute(`conference ${confName} hup all`);
    }

    async muteUnmuteCaller(confName, memberId, mute)
    {
        return await this.eslExecute(`conference ${confName} ${mute ? 'mute' : 'unmute'} ${memberId}`)
    }

    async muteUnmuteAll(confName, mute)
    {
        if (mute)
        {
            console.log(`conference ${confName} mute non_moderator`)
            // don't mute moderators with everyone else
            return await this.eslExecute(`conference ${confName} mute non_moderator`)
        }

        console.log(`conference ${confName} unmute all`);
        return await this.eslExecute(`conference ${confName} unmute all`)
    }
}

module.exports = CallManager;