# BD Conferencing FreeSWITCH Node.js Program

A FreeSWITCH application for running conferences, works in combination with a Firebase back-end.
Requires a working FreeSWITCH installation.

**_PLEASE NOTE BEFORE CONTINUING_**

*_You should not need to follow the below instructions. There is an automated solution which can deploy a Paroli server with minimal manual configuration [here](https://gitlab.com/action-lab-aus/phoneconferencing/freeswitch-deployment). That solution will deploy this repository, install and configure FreeSwitch, install certificates - the works. The manual instructions listed below are very tedious, take a long time to follow, are not actively maintained and are likely out of date. It is extremely unlikely that you will have a good time by staying here. These instructions are to be used as a reference if for some reason the automated solution fails._*

_Here be dragons._

## Installation
Requires FreeSWITCH and Node JS to be installed, as well as some directory permissions to be configured. Fire up a Debian Buster AWS EC2 instance, and create an elastic IP for it. Use that elastic IP to register the instance to a domain name. Then do the following:

### FreeSWITCH Installation
You can install FreeSwitch from a package, but then the directories will be different from 90% of online documentation. Better to download and compile from source. Download and install FreeSWITCH, the Javascript event-socket library, and create TLS certs:

    sudo -i
    apt-get update
    apt-get -y upgrade
    apt-get install -y wget ca-certificates git gnupg2
    wget -O - https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -
    echo "deb http://files.freeswitch.org/repo/deb/debian-unstable/ `lsb_release -sc` main" > /etc/apt/sources.list.d/freeswitch.list
    echo "deb-src http://files.freeswitch.org/repo/deb/debian-unstable/ `lsb_release -sc` main" >> /etc/apt/sources.list.d/freeswitch.list
    apt-get update
    apt-get build-dep freeswitch
    git config --global pull.rebase true
    cd /usr/src/
    git clone https://github.com/signalwire/freeswitch.git freeswitch
    cd freeswitch
    ./bootstrap.sh -j
    nano modules.conf <- uncomment mod_esl & mod_v8
    ./configure
    make
    make install
    make cd-sounds-install cd-moh-install
    cd /usr/local
    groupadd freeswitch
    adduser --disabled-password --quiet --system --home /usr/local/freeswitch --gecos "FreeSWITCH open source softswitch" --ingroup freeswitch freeswitch
    chown -R freeswitch:freeswitch /usr/local/freeswitch/
    chmod -R ug=rwX,o= /usr/local/freeswitch/
    chmod -R u=rwx,g=rx /usr/local/freeswitch/bin/*

Now copy ./ConfigFiles/freeswitch.service to /etc/systemd/system/. Then open /etc/freeswitch/vars.xml and do the following:

- change the default password
- set the "sound_prefix" variable to equal just "$${sounds_dir}", removing "/en/us/callie"
- update the 'internal_ssl_enable' and 'external_ssl_enable' to be true

Then run:

    systemctl daemon-reload
    systemctl start freeswitch
    systemctl enable freeswitch

    sudo apt-get update && sudo apt-get install -y freeswitch-meta-all
    sudo apt-get install freeswitch-mod-v8

### Securing with Fail2Ban

If you're unable to lock the server down to only accepting requests from particular IP addresses, adding Fail2Ban will help mitigate authentication spam:

    apt-get install fail2ban
    cd /usr/src/freeswitch/src/mod/applications
    git clone https://github.com/kurtabersold/freeswitch-mod_fail2ban.git mod_fail2ban
    cd mod_fail2ban/
    make && make install
    cp fail2ban.conf.xml /usr/local/freeswitch/conf/autoload_configs/
    cp jail-freeswitch.conf /etc/fail2ban/jail.d/freeswitch.conf
    cp filter-freeswitch.conf  /etc/fail2ban/filter.d/freeswitch.conf
    systemctl restart fail2ban
    cd /usr/local/freeswitch/bin
    ./fs_cli -x 'reloadxml'

### Node JS Installation
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
    . ~/.nvm/nvm.sh
    nvm install node

Clone this repo, and then install all packages with 'npm install'.

### User Configuration
FreeSWITCH will create its own user and group. Many of the files it creates (e.g. recordings) will only be only be accessible by users in the 'freeswitch' group. This program therefore needs to be run as a member of that group when it is launched.

Set your current user (e.g. 'admin') as a member of the 'freeswitch' group. Close and re-open the SSH session for it to take effect.

    sudo usermod -g freeswitch admin

To allow the app to auto-download audio files and help with setting up the below files over FTP, you should also change the permissions of the FS folders to enable group write access:

    sudo chmod -R 770 /usr/local/freeswitch

### FreeSWITCH Files
Some files need to be moved from this repo into the folders FreeSWITCH creates when it is installed:
- Place ConfigFiles/paroli_inbound.xml in /usr/local/freeswitch/conf/dialplan/public/
- Replace /usr/local/freeswitch/conf/dialplan/default.xml with ConfigFiles/default.xml 
- Change the password in ConfigFiles/1000.xml to one DIFFERENT to your global Freeswitch password, then delete all of the files from /usr/local/freeswitch/conf/directory/default/ and put it in there. 
- Place ConfigFiles/paroli_ivr.xml in /usr/local/freeswitch/conf/ivr_menus/
- Place conference.conf.xml in /usr/local/freeswitch/conf/autoload_configs, replacing the existing file
- Add sip trunk connection details to ConfigFiles/pstn_gateway.xml and place in /usr/local/freeswitch/conf/sip_profiles/external.
- Place ConfigFiles/paroli_phrases.xml in /usr/local/freeswitch/conf/lang/en/ivr/ (and whichever other languages you want to support)
- Replace :/usr/local/freeswitch/conf/lang/en/en.xml with ConfigFiles/en.xml
- Add any additional languages you added to /usr/local/freeswitch/conf/freeswitch.xml

When you alter any FreeSWITCH XML files they need to be reloaded through its CLI:

    /usr/local/freeswitch/bin/fs_cli
    reloadxml
    ...

The app also requires configured versions of 'firebase_service_acc_privatekey.json' (from the project settings -> service accounts menu in Firebase) and 'siptrunk.json' to be in the 'keys' directory. Move the keys directory to the level above the repo (../keys)

### Configure WebRTC

Note that for WebRTC to work, you need a domain name to use with a certificate (doesn't work with just an IP). I made a subdomain to the project site and used that to serve the WebRTC websocket (e.g. fs.caritas.paroli.live)
Replace {fs.yourdomain.com} in these instructions with your full (sub)domain and TLD.

Run the following:

    sudo -i
    echo "deb http://ftp.debian.org/debian stretch-backports main" >> /etc/apt/sources.list
    apt update && apt upgrade
    apt install python-certbot-apache -t stretch-backports
    apt install apache2

Replace /etc/apache2/sites-available/default-ssl.conf with ./ConfigFiles/default-ssl.conf

Make a new file 'nano /etc/apache2/sites-available/{fs.yourdomain.com}.conf' and put the following in it:

    servername {fs.yourdomain.com}

Check your localhost responds to pinging this domain to make sure this is working:

    ping {fs.yourdomain.com}

Now to generate a certificate with Let's Encrypt. Run the following and follow the on-screen instructions:

    systemctl reload apache2
    certbot --apache -d {fs.yourdomain.com}

Once complete, you will be given the location of your new certificate and key (e.g. /etc/letsencrypt/live/{fs.yourdomain.com}/). FreeSwitch wants these as a single file.
Store the combined cert and key in the FreeSwitch TLS config folder:

    cat /etc/letsencrypt/live/{fs.yourdomain.com}/fullchain.pem /etc/letsencrypt/live/{fs.yourdomain.com}/privkey.pem > /usr/local/freeswitch/certs/wss.pem

Replace /usr/local/freeswitch/conf/autoload_configs/verto.conf.xml with ./ConfigFiles/verto.conf.xml, changing the profile name field to your domain. 

Uncomment the 'jsonrpc-allowed-event-channel' line in /etc/freeswitch/directory/default.xml

### Auto-Launch
The app should now work when manually run using 'npm start'. Test before continuing.

To deploy permanently, we'll use PM2 to auto-launch our app using the admin account. Install PM2:

    npm install pm2@latest -g

Navigate into the repo and run:

    pm2 start paroli.js

Generate a startup script:

    pm2 startup systemd

Run the last line of the output (sudo env PATH...)

Save the PM2 configuration:

    pm2 save

Restart the server:

    sudo reboot

SSH back in and start the service:

    sudo systemctl start pm2-admin

It can be restarted with: 

    pm2 restart paroli

To view the process output in realtime:

    pm2 logs paroli


### References

Instructions for setting up FreeSwitch adapted from:

https://freeswitch.org/confluence/display/FREESWITCH/Debian+10+Buster

Instructions for setting up certificates and WebRTC taken from:

https://dopensource.com/2017/01/21/setting-up-freeswitch-webrtc-functionality/
