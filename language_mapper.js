
const data = require('./countriesData.json');
const fs = require('fs');
const path = require("path");
const langSettings = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../keys/languages.json'), 'utf8')); 

exports.GetCountryDialCode = function(phone) 
{
    // longest country codes are 4 numbers, plus '+'.
    // Check from longest to shortest.
    for(var i = 5; i > 1; i--)
    {
        var thisCode = phone.substring(0, i);
        if(data.dialCodeToCountry[thisCode])
        {
            return thisCode;
        }
    }

    return null;
}

exports.MapPhoneToLanguage = function(phone) 
{
    if(phone.charAt(0) !== '+')
    {
        // Need a full country code
        console.log(`No country code given for ${phone}, using default lang ${langSettings.default}`)
        return langSettings.default;
    }

    let dialCode = this.GetCountryDialCode(phone);
    return this.MapCountryToLanguage(data.dialCodeToCountry[dialCode]);
}

exports.MapCountryToLanguage = function(countryCode) 
{
    countryCode = countryCode.toLowerCase();

    if (typeof data.countryToLanguage[countryCode] !== 'undefined') 
    {
        var langs = data.countryToLanguage[countryCode].split(',');
        for(var i = 0; i < langs.length; i++)
        {
            if(langSettings.supported.includes(langs[i]))
            {
                console.debug(`Using ${langs[i]} for caller from ${countryCode}`)
                return langs[i];
            }            
        } 
    } 

    console.debug(`No supported language found for ${countryCode}, defaulting to ${langSettings.default}`)
    return langSettings.default;
}